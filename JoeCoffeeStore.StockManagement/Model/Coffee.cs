﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace JoeCoffeeStore.StockManagement.Model
{
    public class Coffee : INotifyPropertyChanged
    {
        private int _coffeeId;
        private string _coffeeName;
        private int _price;
        private string _description;
        private Country _originCountry;
        private bool _inStock;
        private int _amountInStock;
        private DateTime _firstAddedToStockDate;

        public int CoffeeId { get => _coffeeId; set { _coffeeId = value; RaisePropertyChanged(); } }
        public string CoffeeName { get => _coffeeName; set { _coffeeName = value; RaisePropertyChanged(); } }
        public int Price { get => _price; set { _price = value; RaisePropertyChanged(); } }
        public string Description { get => _description; set { _description = value; RaisePropertyChanged(); } }
        public Country OriginCountry { get => _originCountry; set { _originCountry = value; RaisePropertyChanged(); } }
        public bool InStock { get => _inStock; set { _inStock = value; RaisePropertyChanged(); } }
        public int AmountInStock { get => _amountInStock; set { _amountInStock = value; RaisePropertyChanged(); } }
        public DateTime FirstAddedToStockDate { get => _firstAddedToStockDate; set { _firstAddedToStockDate = value; RaisePropertyChanged(); } }
        public int ImageId { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
