﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace JoeCoffeeStore.StockManagement.Converter
{
    class ImageConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(ImageSource))
            {
                throw new InvalidOperationException("Target type must be System.Windows.Media.ImageSource.");
            }

            try
            {

                Uri uri = new Uri($"pack://application:,,,/Resources/coffee{value}.jpg");
                BitmapImage img = new BitmapImage(uri);
                return img;
            }
            catch
            {

                return DependencyProperty.UnsetValue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
