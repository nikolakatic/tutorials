﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HelloWPF
{
    static class Helpers
    {
        public static bool IsWindowOpen<T>(string name="") where T : Window
        {
            if(string.IsNullOrEmpty(name))
            {
                return Application.Current.Windows.OfType<T>().Any();
            }
            else
            {
                return Application.Current.Windows.OfType<T>().Any(w => w.Name.Equals(name));
            }
        }
    }
}
