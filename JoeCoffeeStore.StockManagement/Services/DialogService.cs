﻿using JoeCoffeeStore.StockManagement.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace JoeCoffeeStore.StockManagement.Services
{
    /// <summary>
    /// Used for opening the detail view
    /// </summary>
    class DialogService: IDialogService
    {
        Window coffeeDetailView = null;

        public void ShowDialog()
        {
            coffeeDetailView = new CoffeeDetailView();
            coffeeDetailView.ShowDialog();
        }

        public void CloseDialog()
        {
            coffeeDetailView?.Close();
        }
    }
}
