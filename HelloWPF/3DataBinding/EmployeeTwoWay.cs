﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HelloWPF._3DataBinding
{
    public class EmployeeTwoWay : INotifyPropertyChanged
    {
        private string _name;
        private string _title;

        public string Name { get { return _name; } set { _name = value; OnPropertyChanged(); } }
        public string Title { get { return _title; } set { _title = value; OnPropertyChanged(); } }

        public static EmployeeTwoWay GetEmployee()
        {
            return new EmployeeTwoWay()
            {
                Name = "Tom Hanks",
                Title = "Actor"
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string caller = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
        }
    }
}