﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HelloWPF._3DataBinding
{
    internal class EmployeeINPC : INotifyPropertyChanged
    {
        private string _name;
        private string _title;

        public string Name { get { return _name; } set { _name = value; OnPropertyChanged(); } }
        public string Title { get { return _title; } set { _title = value; OnPropertyChanged(); } }

        public static EmployeeINPC GetEmployee()
        {
            return new EmployeeINPC()
            {
                Name = "Tom Hanks",
                Title = "Actor"
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string caller = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(caller));
            }
        }
    }
}