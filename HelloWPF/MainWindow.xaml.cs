﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HelloWPF._1PanelsTutorial;
using HelloWPF._2Controls;
using HelloWPF._3DataBinding;
using HelloWPF._4Async;
using HelloWPF.MsExam;
using HelloWPF._5AdvancedControls;
using HelloWPF._6StylesAndTemplates;

namespace HelloWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string buttonText = ((Button)sender).Content.ToString();
            if (Helpers.IsWindowOpen<Window>(buttonText))
            {
                switch (buttonText)
                {
                    case "Layout With Grids":
                        Application.Current.Windows.OfType<_3LayoutWithWrapPanel>().First(w => w.Name.Equals("layoutWithGrids")).Focus();
                        break;
                    case "Layout With Stack Panel":
                        Application.Current.Windows.OfType<_3LayoutWithWrapPanel>().First(w => w.Name.Equals("layoutWithStackPanel")).Focus();
                        break;
                    case "Layout With Wrap Panel":
                        Application.Current.Windows.OfType<_3LayoutWithWrapPanel>().First(w => w.Name.Equals("layoutWithWrapPanel")).Focus();
                        break;
                    case "Layout With Dock Panel":
                        Application.Current.Windows.OfType<_4LayoutWithDockPanel>().First(w => w.Name.Equals("layoutWithDockPanel")).Focus();
                        break;
                    case "Layout With Canvas":
                        Application.Current.Windows.OfType<_5LayoutWithCanvas>().First(w => w.Name.Equals("layoutWithCanvas")).Focus();
                        break;
                }
            }
            else
            {
                switch (buttonText)
                {
                    case "Layout With Grids":
                        _1LayoutWithGrids window = new _1LayoutWithGrids();
                        window.Show();
                        break;
                    case "Layout With Stack Panel":
                        _2LayoutWithStackPanel window1 = new _2LayoutWithStackPanel();
                        window1.Show();
                        break;
                    case "Layout With Wrap Panel":
                        _3LayoutWithWrapPanel window2 = new _3LayoutWithWrapPanel();
                        window2.Show();
                        break;
                    case "Layout With Dock Panel":
                        _4LayoutWithDockPanel window3 = new _4LayoutWithDockPanel();
                        window3.Show();
                        break;
                    case "Layout With Canvas":
                        _5LayoutWithCanvas window4 = new _5LayoutWithCanvas();
                        window4.Show();
                        break;
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (Helpers.IsWindowOpen<Window>("ControlsMainWindow"))
            {
                Application.Current.Windows.OfType<ControlsMainWindow>().First(w => w.Name.Equals("controlsMainWindow")).Focus();
            }
            else
            {
                ControlsMainWindow window = new ControlsMainWindow();
                window.Show();
            }
        }

        private void Button_Click_MS(object sender, RoutedEventArgs e)
        {
            string buttonText = ((Button)sender).Content.ToString();

            if (Helpers.IsWindowOpen<Window>(buttonText))
            {
                switch (buttonText)
                {
                    case "Random Averages":
                        Application.Current.Windows.OfType<RandomAverages>().First(w => w.Name.Equals("randomAveragesWindow")).Focus();
                        break;
                    case "Webpages Viewer":
                        Application.Current.Windows.OfType<WebpageViewer>().First(w => w.Name.Equals("webpageViewerWindow")).Focus();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                Window window;
                switch (buttonText)
                {
                    case "Random Averages":
                        window = new RandomAverages();
                        window.Show();
                        break;
                    case "Webpages Viewer":
                        window = new WebpageViewer();
                        window.Show();
                        break;
                    default:
                        break;
                }

            }
        }

        private void DataBinding_Click(object sender, RoutedEventArgs e)
        {
            string buttonName = ((Button)sender).Name.ToString();
            if (Helpers.IsWindowOpen<Window>(buttonName))
            {
                switch (buttonName)
                {
                    case "DataBindingOneWay":
                        Application.Current.Windows.OfType<OneWay>().First(w => w.Name.Equals("DataBindingOneWay")).Focus();
                        break;
                    case "DataBindingINPC":
                        Application.Current.Windows.OfType<INPC>().First(w => w.Name.Equals("DataBindingINPC")).Focus();
                        break;
                    case "DataBindingTwoWay":
                        Application.Current.Windows.OfType<TwoWay>().First(w => w.Name.Equals("DataBindingTwoWay")).Focus();
                        break;
                    case "DataBindingList":
                        Application.Current.Windows.OfType<TwoWay>().First(w => w.Name.Equals("DataBindingList")).Focus();
                        break;
                    case "DataBindingElement":
                        Application.Current.Windows.OfType<TwoWay>().First(w => w.Name.Equals("DataBindingElement")).Focus();
                        break;
                    case "DataBindingDataConversion":
                        Application.Current.Windows.OfType<TwoWay>().First(w => w.Name.Equals("DataBindingDataConversion")).Focus();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                Window window;
                switch (buttonName)
                {
                    case "DataBindingOneWay":
                        window = new OneWay();
                        window.Show();
                        break;
                    case "DataBindingINPC":
                        window = new INPC();
                        window.Show();
                        break;
                    case "DataBindingTwoWay":
                        window = new TwoWay();
                        window.Show();
                        break;
                    case "DataBindingList":
                        window = new ListDemo();
                        window.Show();
                        break;
                    case "DataBindingElement":
                        window = new ElementBinding();
                        window.Show();
                        break;
                    case "DataBindingDataConversion":
                        window = new DataConversion();
                        window.Show();
                        break;
                    default:
                        break;
                }
            }
        }

        private void Button_Click_Async(object sender, RoutedEventArgs e)
        {
            string buttonName = ((Button)sender).Name.ToString();
            if (Helpers.IsWindowOpen<Window>(buttonName))
            {
                switch (buttonName)
                {
                    case "Async":
                        Application.Current.Windows.OfType<OneWay>().First(w => w.Name.Equals("Async")).Focus();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                Window window;
                switch (buttonName)
                {
                    case "Async":
                        window = new AsyncProgramming();
                        window.Show();
                        break;
                    default:
                        break;
                }
            }
        }

        private void Button_Click_Advanced(object sender, RoutedEventArgs e)
        {
            string buttonName = ((Button)sender).Name.ToString();
            if (Helpers.IsWindowOpen<Window>(buttonName))
            {
                switch (buttonName)
                {
                    case "TabCtrl":
                        Application.Current.Windows.OfType<TabCtrl>().First(w => w.Name.Equals("TabCtrl")).Focus();
                        break;
                    case "DataGrid":
                        Application.Current.Windows.OfType<TabCtrl>().First(w => w.Name.Equals("DataGrid")).Focus();
                        break;
                    case "TreeView":
                        Application.Current.Windows.OfType<TabCtrl>().First(w => w.Name.Equals("TreeView")).Focus();
                        break;
                    case "StatusBar":
                        Application.Current.Windows.OfType<TabCtrl>().First(w => w.Name.Equals("StatusBar")).Focus();
                        break;
                    case "Menu":
                        Application.Current.Windows.OfType<TabCtrl>().First(w => w.Name.Equals("StatusBar")).Focus();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                Window window;
                switch (buttonName)
                {
                    case "TabCtrl":
                        window = new TabCtrl();
                        window.Show();
                        break;
                    case "DataGrid":
                        window = new DataGridCtrl();
                        window.Show();
                        break;
                    case "TreeView":
                        window = new TreeViewCtrl();
                        window.Show();
                        break;
                    case "StatusBar":
                        window = new StatusBarCtrl();
                        window.Show();
                        break;
                    case "Menu":
                        window = new MenuCtrl();
                        window.Show();
                        break;
                    default:
                        break;
                }
            }
        }

        private void Button_Click_StylesAndTemplates(object sender, RoutedEventArgs e)
        {
            string buttonName = ((Button)sender).Name.ToString();
            if (Helpers.IsWindowOpen<Window>(buttonName))
            {
                switch (buttonName)
                {
                    case "Styles":
                        Application.Current.Windows.OfType<TabCtrl>().First(w => w.Name.Equals("Styles")).Focus();
                        break;
                    case "Templates":
                        Application.Current.Windows.OfType<TabCtrl>().First(w => w.Name.Equals("Templates")).Focus();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                Window window;
                switch (buttonName)
                {
                    case "Styles":
                        window = new StylesDemo();
                        window.Show();
                        break;
                    case "Templates":
                        window = new TemplateDemo();
                        window.Show();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
