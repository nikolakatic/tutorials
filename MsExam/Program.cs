﻿using MsExam.Chapter1;
using MsExam.Chapter1.Skill1._2;
using MsExam.Chapter1.Skill1._4;
using MsExam.Chapter1.Skill1._5;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MsExam
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Chapter 1: Manage program flow
            #region Skill 1.1: Implement multithreading and asynchronous processing
            //Parallel.Invoke
            //Invoke.Execute();

            //Parallel.Foreach
            //Foreach.Execute();

            //Parallel.For
            //MyFor.Execute();

            //Managing Parallel.For and Parallel.Foreach
            //Managing.Execute();

            //Parallel LINQ
            //Plinq.Execute();

            //For All
            //ForAll.Execute();

            //PLINQ Exceptions
            //Plinq.Execute();

            //Tasks
            //---start a task and run it to completion
            //Tasks.Execute1();
            //---task returning a value
            //Tasks.Execute2();
            //---wait for all tasks to complete
            //Tasks.Execute3();
            //---continuation tasks
            //Tasks.Execute4();
            //---Child tasks
            //Tasks.Execute5();
            //---Creating threads
            //Tasks.Execute6();
            //---Aborting threads
            //Tasks.Execute7();
            //---Aborting threads in a smarter way
            //Tasks.Execute8();
            //---Thread synchronization using join
            //Tasks.Execute9();
            //---ThreadLocal usage
            //Tasks.Execute10();
            //---Thread execution context. Display information about the thread
            //Tasks.Execute11();
            //---Thread pools
            //Tasks.Execute12();

            //Concurrent collections
            //Blocking collections
            //ConcurrentCollections.ExecuteBlockingCollection();
            //ConcurrentCollections.WrapConcurrentCollections();
            //ConcurrentCollections.ConcurrentQueueExample();
            //ConcurrentCollections.ConcurrentDictionaryExample();
            #endregion

            #region Skill 1.2: Manage multithreading

            //ResourceSynchronization.SingleTaskSumming();
            //it got a bit better
            //ResourceSynchronization.BadTaskInteraction();
            //ResourceSynchronization.Monitors();
            //ResourceSynchronization.SequentialLocking();
            //ResourceSynchronization.CancelTaskExample();
            //ResourceSynchronization.CancelWithException();

            #endregion Skill 1.2: Manage multithreading

            #region Skill 1.4: Create and implement events and callbacks

            //Delegates.PublishSubscribeUnsubscribe();
            //Delegates.AlarmWithEventHandler();
            //Delegates.AlarmWithPayload();
            //Delegates.AlarmWithAggregateException();
            //CreateDelegates.CustomDelegates();

            #endregion Skill 1.4: Create and implement events and callbacks

            #region Skill 1.5: Implement exception handling

            //ExceptionTypes.ExceptionObject();
            ExceptionTypes.CreatingCustomExceptions();

            #endregion Skill 1.5: Implement exception handling

            #endregion Chapter 1: Manage program flow
        }
    }
}
