﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HelloWPF._5AdvancedControls
{
    /// <summary>
    /// Interaction logic for StatusBarCtrl.xaml
    /// </summary>
    public partial class StatusBarCtrl : Window
    {
        public StatusBarCtrl()
        {
            InitializeComponent();
        }

        private void help_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("This is a help dialog");
        }
    }
}
