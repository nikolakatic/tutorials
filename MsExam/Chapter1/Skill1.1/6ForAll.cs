﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MsExam.Chapter1
{
    class ForAll
    {
        public static void Execute()
        {
            Person[] people = Person.People;
            var result = from person in people.AsParallel()
                         where person.City == "Seattle"
                         select person;
            result.ForAll(person => Console.WriteLine(person.Name));

            Console.WriteLine("Finished Processing. Press any key to continue.");
            Console.ReadKey();
        }
    }
}
