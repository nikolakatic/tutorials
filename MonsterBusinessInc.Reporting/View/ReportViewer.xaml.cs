﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MonsterBusinessInc.Reporting.View
{
    /// <summary>
    /// Interaction logic for ReportViewer.xaml
    /// </summary>
    public partial class ReportViewer : UserControl
    {
        public ReportViewer()
        {
            InitializeComponent();
        }

        private void reportViewer_RenderingComplete(object sender, Microsoft.Reporting.WinForms.RenderingCompleteEventArgs e)
        {

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("ID", typeof(int)));
            dt.Columns.Add(new DataColumn("Name", typeof(string)));
            dt.Columns.Add(new DataColumn("City", typeof(string)));
            dt.Columns.Add(new DataColumn("OrderAmount", typeof(int)));
            
            DataRow dr = dt.NewRow();
            dr["ID"] = 1;
            dr["Name"] = "CK Nitin";
            dr["City"] = "New York";
            dr["OrderAmount"] = 100;
            dt.Rows.Add(dr);
            DataRow dr2 = dt.NewRow();
            dr2["ID"] = 2;
            dr2["Name"] = "N K";
            dr2["City"] = "Ankara";
            dr2["OrderAmount"] = 200;
            dt.Rows.Add(dr2);

            ReportDataSource reportDataSource = new ReportDataSource() {
                //name of the DataSet we set in .rdlc!!!
                Name = "CustomerDataSet",
                Value=dt
            };

            //Path of the rdlc file
            ReportViewerPanel.LocalReport.ReportPath = @"D:\projects\Tutorials\MonsterBusinessInc.Reporting\CustomerReport.rdlc";

            ReportViewerPanel.LocalReport.DataSources.Add(reportDataSource);
            ReportViewerPanel.RefreshReport();
        }
    }
}
