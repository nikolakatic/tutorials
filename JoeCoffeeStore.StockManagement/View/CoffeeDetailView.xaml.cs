﻿using MahApps.Metro.Controls;

namespace JoeCoffeeStore.StockManagement.View
{
    /// <summary>
    /// Interaction logic for CoffeeDetailView.xaml
    /// </summary>
    public partial class CoffeeDetailView : MetroWindow
    {
        //public Coffee SelectedCoffee { get; set; }

        public CoffeeDetailView()
        {
            InitializeComponent();
            //Loaded += CoffeeDetailView_Loaded;
        }

        //void CoffeeDetailView_Loaded(object sender, RoutedEventArgs e)
        //{
        //    //LoadData();
        //    DataContext = SelectedCoffee;
        //}

        //private void LoadData()
        //{
        //    CoffeeNameLabel.Content = SelectedCoffee.CoffeeName;
        //    CoffeeIdTextBox.Text = SelectedCoffee.CoffeeId.ToString();
        //    CoffeeDescriptionTextBox.Text = SelectedCoffee.Description;
        //    CoffeePriceTextBox.Text = SelectedCoffee.Price.ToString();
        //    StockAmmountTextBox.Text = SelectedCoffee.AmountInStock.ToString();
        //    FirstTimeAddedTextBox.Text = SelectedCoffee.FirstAddedToStockDate.ToShortDateString();
        //    if(SelectedCoffee is SuperiorCoffee)
        //    {
        //        ExtraDescriptionTextBox.Text = (SelectedCoffee as SuperiorCoffee).ExtraDescription;
        //    }
        //    else
        //    {
        //        ExtraDescriptionTextBox.Text = "";
        //    }

        //    Uri uri = new Uri($"pack://application:,,,/Resources/coffee{SelectedCoffee.ImageId}.jpg");
        //    BitmapImage img = new BitmapImage(uri);
        //    CoffeeImage.Source = img;
        //}

        //private void SaveCoffeeButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.Close();
        //}

        //private void DeleteCoffeeButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.Close();
        //}

        //private void ChangeCoffeeButton_Click(object sender, RoutedEventArgs e)
        //{
        //    SelectedCoffee.CoffeeName = "Something really expensive";
        //    SelectedCoffee.Price = 1000;
        //}
    }
}