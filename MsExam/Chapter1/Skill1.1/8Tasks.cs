﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MsExam.Chapter1
{
    class Tasks: Work
    {
        //Use Execute and Execute1 if you want to start tasks and have them run to completion
        public static void Execute()
        {
            Task newTask = new Task(() => DoWork());
            newTask.Start();
            newTask.Wait();
            Finished();
        }

        public static void Execute1()
        {
            Task newTask = Task.Run(() => DoWork());
            newTask.Wait();
            Finished();
        }

        //task returning a value
        public static void Execute2()
        {
            Task<int> task = Task.Run(() =>
            {
                return CalculateResult();
            });
            Console.WriteLine(task.Result);
            Finished();
        }

        //task wait all
        public static void Execute3()
        {
            Task[] tasks = new Task[10];
            for (int i = 0; i < 10; i++)
            {
                int taskNum = i;
                tasks[i] = Task.Run(() => DoWork(taskNum));
            }
            Task.WaitAll(tasks);
            Finished();
        }

        //continuation tasks. Set up so that the app waits for the continuation task to finish before continuing.
        public static void Execute4()
        {
            Task task = Task.Run(() =>
            {
                Thread.Sleep(1000);
                Console.WriteLine("Hello");
            });
            Task continuationTask = task.ContinueWith((prevTask) =>
            {
                Thread.Sleep(1000);
                Console.WriteLine("World");
            }, TaskContinuationOptions.OnlyOnRanToCompletion); //overload method that specfies when a given continuation task can run.
            Task exceptionTask = task.ContinueWith((prevTask) =>
            {
                Console.WriteLine("An exception has been thrown.");
            }, TaskContinuationOptions.OnlyOnFaulted);
            Task.WaitAll(continuationTask);
            Finished();
        }

        //child tasks
        public static void Execute5()
        {
            var parent = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("ParentStarts");
                for (int i = 0; i < 10; i++)
                {
                    int taskNo = i;
                    Task.Factory.StartNew((x) =>
                        {
                            Console.WriteLine("Child {0} starting.", x);
                            Thread.Sleep(2000);
                            Console.WriteLine("\tChild {0} finished.", x);
                        },
                    taskNo,
                    TaskCreationOptions.AttachedToParent
                    );
                }
            });
            parent.Wait();
            Finished();
        }

        //Threads
        //create and run a thread with some data passed to it. Thread accepts an object that encapsulates passed data.
        public static void Execute6()
        {
            Thread thread = new Thread((data) =>
            {
                Console.WriteLine("Hello from the thread. Data passed: {0}", data);
                Thread.Sleep(2000);
            });
            thread.Start(101);
            Finished();
        }

        //Abort a thread. This way is not good because it can leave a program in an ambiguous state
        public static void Execute7()
        {
            Thread clockThread = new Thread(() =>
            {
                string clock = "Tick";
                while (true)
                {
                    Console.WriteLine(clock);
                    clock = clock == "Tick" ? "Tock" : "Tick";
                    Thread.Sleep(1000);
                }
            });
            clockThread.Start();

            Console.WriteLine("Press any key to stop the clock.");
            Console.ReadKey();
            clockThread.Abort();
            Finished();
        }

        //Abort a thread. This is a better way to abort a thread. Using a shared flag variable
        static bool tickRunning;
        public static void Execute8()
        {
            tickRunning = true;
            Thread clockThread = new Thread(() =>
            {
                string clock = "Tick";
                while (tickRunning)
                {
                    Console.WriteLine(clock);
                    clock = clock == "Tick" ? "Tock" : "Tick";
                    Thread.Sleep(1000);
                }
            });
            clockThread.Start();
            Console.WriteLine("Press any key to stop the clock.");
            Console.ReadKey();
            tickRunning = false;
            Finished();
        }

        //Thread synchronization using join. Main thread will wait for the threadToWaitFor to complete before continuing with the execution.
        public static void Execute9()
        {
            Thread threadToWaitFor = new Thread(() =>{
                Console.WriteLine("Thread starting.");
                Thread.Sleep(2000);
                Console.WriteLine("Thread finished.");
            });
            threadToWaitFor.Start();
            Console.WriteLine("Joining thread");
            threadToWaitFor.Join();
            Finished();
        }

        //ThreadLocal class. Used to initialize the local data for each thread in the same way
        private static readonly ThreadLocal<Random> RandomGenerator = new ThreadLocal<Random>(() =>
        {
            return new Random(2);
        });
        public static void Execute10()
        {
            Thread t1 = new Thread(() =>
            {
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine("t1: {0}", RandomGenerator.Value.Next(10));
                    Thread.Sleep(500);
                }
            });
            Thread t2 = new Thread(() =>
            {
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine("t2: {0}", RandomGenerator.Value.Next(10));
                    Thread.Sleep(500);
                }
            });

            t1.Start();
            t2.Start();
            t1.Join();
            t2.Join();
            Finished();
        }

        //Thread execution context. Display or set information about the thread
        private static void DisplayThread(Thread t)
        {
            Console.WriteLine("Name: {0}", t.Name);
            Console.WriteLine("Culture: {0}", t.CurrentCulture);
            Console.WriteLine("Priority: {0}", t.Priority);
            Console.WriteLine("Context: {0}", t.ExecutionContext);
            Console.WriteLine("IsBackground?: {0}", t.IsBackground);
            Console.WriteLine("IsPool?: {0}", t.IsThreadPoolThread);
        }
        public static void Execute11()
        {
            Thread.CurrentThread.Name = "Main method thread";
            DisplayThread(Thread.CurrentThread);
        }

        //Thread pools
        public static void Execute12()
        {
            for (int i = 0; i < 50; i++)
            {
                int threadNumber = i;
                ThreadPool.QueueUserWorkItem(state => DoWork(threadNumber));
            }
            Console.ReadKey();
        }

    }
}
