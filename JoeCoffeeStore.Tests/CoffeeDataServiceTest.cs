﻿using System;
using JoeCoffeeStore.StockManagement.DAL;
using JoeCoffeeStore.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JoeCoffeeStore.StockManagement.Services;

namespace JoeCoffeeStore.Tests
{
    [TestClass]
    public class CoffeeDataServiceTest
    {
        private ICoffeeRepository repository;
        [TestInitialize]
        public void Init()
        {
            repository = new MockRepository();
        }

        [TestMethod]
        public void GetCoffeeDetailTest()
        {
            //Arrange
            var service = new CoffeeDataService();

            //Act
            var coffee = service.GetCoffeeDetail(1);

            //Assert
            Assert.IsNotNull(coffee);

        }
    }
}
