﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MsExam.Chapter1
{
    class Plinq : Work
    {
        public static void Execute()
        {
            Person[] people = Person.People;

            var result = from person in people.AsParallel()
                         //.WithDegreeOfParallelism(4) //maximum number of processors
                         //.WithExecutionMode(ParallelExecutionMode.ForceParallelism //parallelize query whether performance is improved or not
                         //.AsOrdered() //preserve the order of the original. This can slow down the query. Returns a sorted result but doesn't necessarily run the query in order.
                         where person.City == "Seattle"
                         select person;

            var SequentialResult = (from person in people.AsParallel()
                                    where person.City == "Seattle"
                                    orderby (person.Name)
                                    select new
                                    {
                                        Name = person.Name
                                   }).AsSequential().Take(4);

            foreach (var person in result)
            {
                Console.WriteLine(person.Name);
            }

            Console.WriteLine("Finished Processing. Press any key to continue.");
            Console.ReadKey();
        }
    }
}
