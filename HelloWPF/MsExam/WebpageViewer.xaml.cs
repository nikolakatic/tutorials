﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HelloWPF.MsExam
{
    /// <summary>
    /// Interaction logic for WebpageViewer.xaml
    /// </summary>
    public partial class WebpageViewer : Window
    {
        public WebpageViewer()
        {
            InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TextBlockResult.Text = await FetchWebPage(TextBoxUrl.Text);
                LabelInfoText.Content = "Page Loaded.";
            }
            catch (Exception ex)
            {
                LabelInfoText.Content = ex.Message;
            }
        }

        private static async Task<string> FetchWebPage(string url)
        {
            HttpClient httpClient = new HttpClient();
            return await httpClient.GetStringAsync(url);
        }

        //how to create multiple tasks and wait for all of them to complete
        static async Task<IEnumerable<string>> FetchWebPages(string[] urls)
        {
            var tasks = new List<Task<String>>();
            foreach (string url in urls)
            {
                tasks.Add(FetchWebPage(url));
            }
            return await Task.WhenAll(tasks);
        }
    }
}
