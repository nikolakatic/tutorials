﻿namespace JoeCoffeeStore.StockManagement.Services
{
    public interface IDialogService
    {
        void ShowDialog();
        void CloseDialog();
    }
}