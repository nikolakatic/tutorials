﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MsExam.Chapter1.Skill1._2
{
    public class ResourceSynchronization : Shared
    {
        // 1-41
        public static void SingleTaskSumming()
        {

            int[] numbers = Enumerable.Range(0, 50000001).ToArray();

            long total = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                total += numbers[i];
            }
            Console.WriteLine($"The total is {total}.");

            Finished();
        }

        // 1-42
        public static void BadTaskInteraction()
        {
            long sharedTotal = 0;
            int[] items = Enumerable.Range(0, 50000001).ToArray();
            object sharedTotalLock = new object();

            //1-43 Using Locking
            void AddRangeOfValues(int start, int end)
            {
                while (start < end)
                {
                    lock (sharedTotalLock)
                    {
                        sharedTotal = sharedTotal + items[start];
                    }
                    start++;
                }
            }

            // 1-44 Sensible locking
            void AddRangeOfValuesSensible(int start, int end)
            {
                long subtotal = 0;
                while (start < end)
                {
                    subtotal += items[start];
                    start++;
                }
                lock (sharedTotalLock)
                    sharedTotal += subtotal;
            }

            List<Task> tasks = new List<Task>();

            int rangeSize = 1000;
            int rangeStart = 0;

            while (rangeStart < items.Length)
            {
                int rangeEnd = rangeStart + rangeSize;

                if (rangeEnd > items.Length)
                    rangeEnd = items.Length;

                int rs = rangeStart;
                int re = rangeEnd;

                //tasks.Add(Task.Run(() => AddRangeOfValues(rs, re)));
                tasks.Add(Task.Run(() => AddRangeOfValuesSensible(rs, re)));
                rangeStart = rangeEnd;
            }

            Task.WaitAll(tasks.ToArray());

            Console.WriteLine($"The total is {sharedTotal}.");

            Finished();
        }

        // 1-45 Using monitors
        public static void Monitors()
        {
            long sharedTotal = 0;
            int[] items = Enumerable.Range(0, 50000001).ToArray();
            object sharedTotalLock = new object();

            void AddRangeOfValues(int start, int end)
            {
                long subtotal = 0;
                while (start < end)
                {
                    subtotal += items[start];
                    start++;
                }

                Monitor.Enter(sharedTotalLock);
                //if(Monitor.TryEnter(sharedTotalLock)){ //stuff } else { //other stuff }
                try
                {
                    sharedTotal += subtotal;
                }
                finally
                {
                    Monitor.Exit(sharedTotalLock);
                }
            }

            List<Task> tasks = new List<Task>();

            int rangeSize = 1000;
            int rangeStart = 0;

            while (rangeStart < items.Length)
            {
                int rangeEnd = rangeStart + rangeSize;
                if (rangeEnd > items.Length)
                    rangeEnd = items.Length;

                int rs = rangeStart;
                int re = rangeEnd;
                tasks.Add(Task.Run(() => AddRangeOfValues(rs, re)));
                rangeStart = rangeEnd;
            }

            Task.WaitAll(tasks.ToArray());

            Console.WriteLine($"The total is {sharedTotal}.");

            Finished();
        }

        // 1-46 Sequential locking
        public static void SequentialLocking()
        {
            object lock1 = new object();
            object lock2 = new object();

            void Method1()
            {
                lock(lock1)
                {
                    Console.WriteLine("Method 1 got lock 1");
                    Console.WriteLine("Method 1 waiting for lock 2");
                    lock(lock2)
                    {
                        Console.WriteLine("Method 1 got lock 2");
                    }
                    Console.WriteLine("Method 1 released lock 2");
                }
                Console.WriteLine("Method 1 released lock 1");
            }

            void Method2()
            {
                lock(lock2)
                {
                    Console.WriteLine("Method 2 got lock 2");
                    Console.WriteLine("Method 2 waiting for lock 1");
                    lock(lock1)
                    {
                        Console.WriteLine("Method 2 got lock 1");
                    }
                    Console.WriteLine("Method 2 released lock 1");
                }
                Console.WriteLine("Method 2 released lock 2");
            }

            //Method1();
            //Method2();
            //Console.WriteLine("Methods completed.");
            //Finished();

            //1-47 Deadlocked tasks
            Task task1 = Task.Run(() => Method1());
            Task task2 = Task.Run(() => Method2());
            Console.WriteLine("Waiting for task 2");
            task2.Wait();
            Finished();
        }

        // 1-48 using Interlocked class that provides a set of thread-safe operations that can be performed on a variable
        public static void InterlockExample()
        {
            long sharedTotal = 0;
            int[] items = Enumerable.Range(0, 50000001).ToArray();
            object sharedTotalLock = new object();

            void AddRangeOfValues(int start, int end)
            {
                long subtotal = 0;
                while (start < end)
                {
                    subtotal += items[start];
                    start++;
                }
                Interlocked.Add(ref sharedTotal, subtotal);
            }

            List<Task> tasks = new List<Task>();

            int rangeSize = 1000;
            int rangeStart = 0;

            while (rangeStart < items.Length)
            {
                int rangeEnd = rangeStart + rangeSize;
                if (rangeEnd > items.Length)
                    rangeEnd = items.Length;

                int rs = rangeStart;
                int re = rangeEnd;
                tasks.Add(Task.Run(() => AddRangeOfValues(rs, re)));
                rangeStart = rangeEnd;
            }

            Task.WaitAll(tasks.ToArray());

            Console.WriteLine($"The total is {sharedTotal}.");

            Finished();
        }

        // 1-49 cancel a task
        public static void CancelTaskExample()
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            void Clock()
            {
                while(!cancellationTokenSource.IsCancellationRequested)
                {
                    Console.WriteLine("Tick");
                    Thread.Sleep(500);
                }
            }

            Task.Run(() => Clock());
            Console.WriteLine("Press any key to stop the clock");
            Console.ReadKey();
            cancellationTokenSource.Cancel();
            Console.WriteLine("Clock stoped. Press any key to exit.");
            Console.ReadKey();
        }

        // 1-50 Cancel with Exception
        public static void CancelWithException()
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            void Clock(CancellationToken cancellationToken)
            {
                int tickCount = 0;
                while(!cancellationToken.IsCancellationRequested && tickCount < 20)
                {
                    Console.WriteLine("Tick");
                    tickCount++;
                    Thread.Sleep(500);
                }

                // if you run the program in Visual Studio and press a key to interrupt the clock,
                // Visual Studio will report an unhandled exception in the Clock method.
                // If the program was running outside Visual Studio, as it would
                // be when a customer is using the program, the cancellation exception would not be detected
                // and only the exception handler for the AggregateException would run.
                cancellationToken.ThrowIfCancellationRequested();
            }

            Task clock = Task.Run(() => Clock(cancellationTokenSource.Token));

            Console.WriteLine("Press any key to stop the clock.");
            Console.ReadKey();

            if(clock.IsCompleted)
            {
                Console.WriteLine("Clock task completed.");
            }
            else
            {
                try
                {
                    cancellationTokenSource.Cancel();
                    clock.Wait();
                }
                catch (AggregateException ex)
                {
                    Console.WriteLine($"Clock stoped: {ex.InnerExceptions[0].ToString()}");
                }
            }
        }
    }
}
