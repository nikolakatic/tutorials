﻿using JoeCoffeeStore.StockManagement.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoeCoffeeStore.Tests.Mocks
{
    class MockDialogService : IDialogService
    {
        public void CloseDialog()
        {
            
        }

        public void ShowDialog()
        {
            
        }
    }
}
