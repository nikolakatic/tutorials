﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWPF._3DataBinding
{
    public class EmployeeOneWay
    {
        public string Name { get; set; }
        public string Title { get; set; }

        public static EmployeeOneWay GetEmployee()
        {
            EmployeeOneWay emp = new EmployeeOneWay()
            {
                Name = "Tom",
                Title = "Developer"
            };
            return emp;
        }
    }
}
