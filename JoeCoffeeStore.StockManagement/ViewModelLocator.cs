﻿using JoeCoffeeStore.StockManagement.DAL;
using JoeCoffeeStore.StockManagement.Services;
using JoeCoffeeStore.StockManagement.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoeCoffeeStore.StockManagement
{
    public class ViewModelLocator
    {
        private static IDialogService dialogService = new DialogService();
        private static IDataService dataService = new CoffeeDataService( new CoffeeRepository() );

        private static CoffeeOverviewViewModel coffeeOverviewViewModel { get; } = new CoffeeOverviewViewModel(dataService, dialogService);
        private static CoffeeDetailViewModel coffeeDetailViewModel { get; } = new CoffeeDetailViewModel(dataService);

        public static CoffeeOverviewViewModel CoffeeOverviewViewModel { get { return coffeeOverviewViewModel; } }
        public static CoffeeDetailViewModel CoffeeDetailViewModel { get { return coffeeDetailViewModel; } }
    }
}