﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MsExam.Chapter1
{
    class Foreach: Work
    {
        public static void Execute()
        {
            var items = Enumerable.Range(0, 20);
            Parallel.ForEach(items, item =>
            {
                WorkOnItem(item);
            });
            Console.WriteLine("Finished processing. Press any key to continue.");
            Console.ReadKey();
        }
    }
}
