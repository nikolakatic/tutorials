﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HelloWPF.MsExam
{
    /// <summary>
    /// Interaction logic for RandomAverages.xaml
    /// </summary>
    public partial class RandomAverages : Window
    {
        public RandomAverages()
        {
            InitializeComponent();
        }

        private async void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            long noOfValues = long.Parse(textBoxInput.Text);
            textBoxResult.Text = "Calculating...";
            buttonStart.IsEnabled = false;
            double result = await (AsyncComputeAverages(noOfValues));
            buttonStart.IsEnabled = true;
            textBoxResult.Text = result.ToString();
        }

        private double ComputeAverages(long noOfValues)
        {
            double total = 0;
            Random r = new Random();
            for (int i = 0; i < noOfValues; i++)
            {
                total += r.NextDouble();
            }
            return total / noOfValues;
        }

        private Task<double> AsyncComputeAverages(long noOfValues)
        {
            return Task.Run(() => {
                return ComputeAverages(noOfValues);
            });
        }
    }
}
