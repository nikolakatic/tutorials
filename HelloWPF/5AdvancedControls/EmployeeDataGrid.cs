﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace HelloWPF._5AdvancedControls
{
    public class EmployeeDataGrid : INotifyPropertyChanged
    {
        #region members
        private string _name;
        private string _title;
        private bool _wasReelected;
        private Party _affiliation;
        #endregion

        #region properties
        public string Name { get { return _name; } set { _name = value; RaisePropertyChanged(); } }
        public string Title { get { return _title; } set { _title = value; RaisePropertyChanged(); } }
        public bool WasReelected { get { return _wasReelected; } set { _wasReelected = value; RaisePropertyChanged(); } }
        public Party Affiliation { get { return _affiliation; } set { _affiliation = value; RaisePropertyChanged(); } }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged([CallerMemberName] string caller = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
        }

        public static ObservableCollection<EmployeeDataGrid> GetEmployees()
        {
            return new ObservableCollection<EmployeeDataGrid>()
            {
                new EmployeeDataGrid(){ Name="Washington", Title="President 1", WasReelected = true, Affiliation= Party.Independent },
                new EmployeeDataGrid(){ Name="Adams", Title="President 2", WasReelected = false, Affiliation = Party.Federalist },
                new EmployeeDataGrid(){ Name="Jefferson", Title = "President 3", WasReelected = true, Affiliation= Party.DemocratRepublican }
            };
        }
    }

    public enum Party
    {
        Independent,
        Federalist,
        DemocratRepublican
    }
}
