﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace HelloWPF._3DataBinding
{
    class EmployeeConversion : INotifyPropertyChanged
    {
        private string _name;
        private string _title;
        private DateTime _startDate;

        public string Name { get { return _name; } set { _name = value; OnPropertyChanged(); } }
        public string Title { get { return _title; } set { _title = value; OnPropertyChanged(); } }
        public DateTime StartDate { get { return _startDate; } set { _startDate = value; OnPropertyChanged(); } }
        public string ThisIsAProperty { get { return GetCallerMemberName();} }

        //returns "ThisIsAProperty"
        private string GetCallerMemberName([CallerMemberName] string caller = "")
        {
            return caller;
        }

        public static EmployeeConversion GetEmployee()
        {
            return new EmployeeConversion()
            {
                Name = "Tom Hanks",
                Title = "Actor",
                StartDate = new DateTime(2012, 5, 5)
            };
        }

        public static ObservableCollection<EmployeeList> GetEmployees()
        {
            ObservableCollection<EmployeeList> employees = new ObservableCollection<EmployeeList>()
            {
                new EmployeeList(){Name = "Washington", Title="President 1"},
                new EmployeeList(){Name = "Adams", Title="President 2"},
                new EmployeeList(){Name = "Jefferson", Title = "President 3"},
                new EmployeeList(){Name = "Madison", Title="President 4"}
            };
            return employees;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string caller = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
        }
    }
}
