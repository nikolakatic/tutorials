﻿using System;
using JoeCoffeeStore.StockManagement.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JoeCoffeeStore.Tests.Mocks;
using System.Collections.ObjectModel;
using JoeCoffeeStore.StockManagement.ViewModel;
using JoeCoffeeStore.StockManagement.Model;

namespace JoeCoffeeStore.Tests
{
    [TestClass]
    public class CoffeeOverviewViewModelTests
    {
        private IDataService coffeeDataService;
        private IDialogService dialogService;

        [TestInitialize]
        public void Init()
        {
            coffeeDataService = new MockCoffeeDataService();
            dialogService = new MockDialogService();
        }

        [TestMethod]
        public void LoadAllCoffees()
        {
            //Arrange
            ObservableCollection<Coffee> coffees = null;
            var expectedCoffees = coffeeDataService.GetAllCoffees();

            //Act
            var viewModel = new CoffeeOverviewViewModel(this.coffeeDataService, this.dialogService);
            coffees = viewModel.Coffees;

            //Assert
            CollectionAssert.AreEqual(expectedCoffees, coffees);
        }
    }
}
