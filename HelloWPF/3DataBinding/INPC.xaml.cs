﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HelloWPF._3DataBinding
{
    /// <summary>
    /// Interaction logic for INPC.xaml
    /// </summary>
    public partial class INPC : Window
    {
        private EmployeeINPC emp;
        public INPC()
        {
            InitializeComponent();
            emp = new EmployeeINPC()
            {
                Name = "Joe",
                Title = "QA"
            };
            DataContext = emp;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            emp.Name="Jesse";
            emp.Title="Evangelist";
        }
    }
}
