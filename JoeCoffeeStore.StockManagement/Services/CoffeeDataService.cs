﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JoeCoffeeStore.StockManagement.DAL;
using JoeCoffeeStore.StockManagement.Model;

namespace JoeCoffeeStore.StockManagement.Services
{
    public class CoffeeDataService : IDataService
    {
        ICoffeeRepository repo;

        public CoffeeDataService()
        {
            this.repo = new CoffeeRepository();
        }

        public CoffeeDataService(ICoffeeRepository repo)
        {
            this.repo = repo;
        }

        public void DeleteCoffee(Coffee coffee)
        {
            repo.DeleteCoffee(coffee);
        }

        public List<Coffee> GetAllCoffees()
        {
            return repo.GetCoffees();
        }

        public Coffee GetCoffeeDetail(int coffeeId)
        {
            return repo.GetCoffeeById(coffeeId);
        }

        public void UpdateCoffee(Coffee coffee)
        {
            repo.UpdateCoffee(coffee);
        }
    }
}
