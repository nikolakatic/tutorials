﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MsExam.Chapter1
{
    class Invoke: Work
    {
        public static void Execute()
        {
            Parallel.Invoke(() => Task1(), () => Task2());
            Console.WriteLine("Finished processing. Press any key to continue.");
            Console.ReadKey();
        }
    }
}
