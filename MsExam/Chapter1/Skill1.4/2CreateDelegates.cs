﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MsExam.Chapter1.Skill1._4
{
    public class CreateDelegates
    {
        delegate int IntOperation(int a, int b);

        // 1-71
        public static void CustomDelegates()
        {
            // Explicitly create the delegate
            IntOperation op = new IntOperation(Add);
            Console.WriteLine(op(2, 3));

            // Implicit delegate creation from the method
            op = Substract;
            Console.WriteLine(op(3, 2));

            op += Add;
            Console.WriteLine("Both?");
            //Console.WriteLine(op(3,2)); //returna samo rezultat zadnjeg dodanog delegata

            //treba ovako:
            foreach (Delegate handler in op.GetInvocationList())
            {
                Console.WriteLine(handler.DynamicInvoke(3, 2));
            }
            Console.ReadKey();
        }

        private static int Add(int a, int b)
        {
            Console.WriteLine("Add called");
            return a + b;
        }

        private static int Substract(int a, int b)
        {
            Console.WriteLine("Substract called");
            return a - b;
        }
    }
}
