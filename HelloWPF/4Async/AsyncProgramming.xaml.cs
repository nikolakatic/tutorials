﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HelloWPF._4Async
{
    /// <summary>
    /// Interaction logic for AsyncProgramming.xaml
    /// </summary>
    public partial class AsyncProgramming : Window
    {
        public AsyncProgramming()
        {
            InitializeComponent();
            ComputeStuffAsync();
        }

        private async void ComputeStuffAsync()
        {
            for(;;)
            {
                double sum = 0;
                Message.Text = "Computing...";
                await Task.Run(() =>
                {
                    for (int i = 1; i < 200000000; i++)
                    {
                        sum += Math.Sqrt(i);
                    }
                });
                Message.Text = "Sum = " + sum;
                await Task.Run(() =>
                {
                    for (int i = 1; i < 200000000; i++)
                    {
                        sum += Math.Sqrt(i);
                    }
                });
                Message.Text = "Sum = " + sum;
                await Task.Run(() =>
                {
                    for (int i = 1; i < 200000000; i++)
                    {
                        sum += Math.Sqrt(i);
                    }
                });
                Message.Text = "Sum = " + sum;
                await Task.Run(() =>
                {
                    for (int i = 1; i < 200000000; i++)
                    {
                        sum += Math.Sqrt(i);
                    }
                });
                Message.Text = "Sum = " + sum;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Button clicked!");
        }
    }
}
