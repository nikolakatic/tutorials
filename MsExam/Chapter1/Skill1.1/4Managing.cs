﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MsExam.Chapter1
{
    class Managing:Work
    {
        public static void Execute()
        {
            int[] items = Enumerable.Range(0, 500).ToArray();

            ParallelLoopResult result = Parallel.For(0, items.Length,
                (int i, ParallelLoopState loopState) =>
                {
                    if (i == 200)
                        //Break - makes sure that iterations with an index lower than the breaking index are performed.
                        //Stop - might be that iterations with an idex lower than 200 will not be performed.
                        //loopState.Stop();
                        loopState.Break();
                    WorkOnItem(items[i]);
                });
            Console.WriteLine("Completed: {0}", result.IsCompleted);
            Console.WriteLine("Items: {0}", result.LowestBreakIteration);

            Console.WriteLine("Finished processing. Press any key to continue.");
            Console.ReadKey();
        }
    }
}