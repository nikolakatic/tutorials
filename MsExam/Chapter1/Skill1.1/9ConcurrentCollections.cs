﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MsExam.Chapter1
{
    class ConcurrentCollections: Work
    {
        public static void ExecuteBlockingCollection()
        {
            BlockingCollection<int> data = new BlockingCollection<int>(5);

            Task.Run(() =>
            {
                //attempt to add 10 items to the collection - block after fifth
                for (int i = 0; i < 10; i++)
                {
                    data.Add(i);
                    Console.WriteLine("Data {0} added sucessfully.", i);
                }
                //indicate we have no more to add
                data.CompleteAdding();
            });

            Console.ReadKey();
            Console.WriteLine("Reading collection");

            Task t = Task.Run(() =>
            {
                while (!data.IsCompleted)
                {
                    try
                    {
                        int value = data.Take();
                        Console.WriteLine("Data {0} taken sucessfully.", value);
                    }
                    catch (InvalidOperationException)
                    { }
                }
            });
            t.Wait();
            Finished();
        }

        // Blocking collection can be used as a wrapper around other concurrent collection classes 
        // including ConcurrentQueue, ConcurrentStack, and ConcurrentBag

        public static void WrapConcurrentCollections()
        {
            BlockingCollection<int> data = new BlockingCollection<int>(new ConcurrentStack<int>(), 5);

            Task t1 = Task.Run(() =>
            {
                for (int i = 0; i < 10; i++)
                {
                    data.Add(i);
                    Console.WriteLine($"Added {i} to concurrent stack.");
                }
                //indicate we are done adding
                data.CompleteAdding();
            });

            Console.ReadKey();
            Console.WriteLine("Starting reading from collection.");

            Task t2 = Task.Run(() =>
            {
                while(!data.IsCompleted)
                {
                    // ovo mora biti u try catch jer se može dogoditi da t2 pokuša uzeti 
                    // podatak prije negot1 postavi CompleteAdding flag a t1 je gotov sa dodavanjem
                    try
                    {
                        int value = data.Take();
                        Console.WriteLine($"Removed {value} from concurrent stack");
                    }
                    catch (Exception)
                    { }
                }
            });
        }

        // Isti primjer je za concurrent stack i bag, razlika je u tome koji se podatak prvi čita
        public static void ConcurrentQueueExample()
        {
            ConcurrentQueue<string> concurrentQueue = new ConcurrentQueue<string>();
            concurrentQueue.Enqueue("Jessica");
            concurrentQueue.Enqueue("Jones");

            if(concurrentQueue.TryPeek(out string str))
            {
                Console.WriteLine($"Peek: {str}");
            }
            if(concurrentQueue.TryDequeue(out str))
            {
                Console.WriteLine($"Dequeue: {str}");
            }
            Finished();
        }

        public static void ConcurrentDictionaryExample()
        {
            ConcurrentDictionary<string, int> ages = new ConcurrentDictionary<string, int>();

            if(ages.TryAdd("Rob", 21))
                Console.WriteLine("Rob added successfully");
            Console.WriteLine($"Rob's age is {ages["Rob"]}.");
            // update Rob's age to 22 if it's 21
            if(ages.TryUpdate("Rob", 22, 21))
                Console.Write("Update successfull.");
            Console.WriteLine($"Rob's new age is {ages["Rob"]}");
            // increment Rob's age atomically using factory method
            Console.WriteLine("Rob's age updated to {0}.",
                ages.AddOrUpdate("Rob", 1, (name, age) => age = age + 1 ));
            Console.WriteLine($"Rob's new age is {ages["Rob"]}");

            Finished();
        }
    }
}
