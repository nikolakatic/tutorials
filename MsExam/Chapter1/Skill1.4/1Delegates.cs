﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace MsExam.Chapter1.Skill1._4
{
    public class Delegates
    {
        public static void PublishSubscribeUnsubscribe()
        {
            // method that must run when the alarm is raised
            // event handler
            void AlarmListener1()
            {
                Console.WriteLine("AlarmListener1 called");
            }

            // method that must run when the alarm is raised
            // event handler
            void AlarmListener2()
            {
                Console.WriteLine("AlarmListener2 called");
            }

            Alarm alarm = new Alarm();

            alarm.OnAlarmRaised += AlarmListener1;
            alarm.OnAlarmRaised += AlarmListener2;

            alarm.RaiseAlarm();
            Console.WriteLine("Alarm raised!");

            alarm.OnAlarmRaised -= AlarmListener2;

            alarm.RaiseAlarm();
            Console.WriteLine("Alarm raised!");

            Console.ReadKey();
        }

        #region Alarm with event handler
        public static void AlarmWithEventHandler()
        {
            EventHandlerAlarm alarm = new EventHandlerAlarm();
            alarm.OnAlarmRaised += AlarmListenerWithEventHandler1;
            alarm.OnAlarmRaised += AlarmListenerWithEventHandler2;
            alarm.RaiseAlarm();
            Console.WriteLine("Alarm Raised");
            Console.ReadKey();
        }

        // event handler
        private static void AlarmListenerWithEventHandler1(object sender, EventArgs e)
        {
            Console.WriteLine("Alarm listener 1 called.");
        }
        // event handler
        private static void AlarmListenerWithEventHandler2(object sender, EventArgs e)
        {
            Console.WriteLine("Alarm listener 2 called.");
        }
        #endregion Alarm with event handler

        #region Alarm with payload
        public static void AlarmWithPayload()
        {
            PayloadAlarm alarm = new PayloadAlarm();
            alarm.OnAlarmRaised += AlarmListenerWithPayload;
            alarm.RaiseAlarm("Jankomir");
            Console.WriteLine("Alarm raised");
            Console.ReadKey();
        }

        // event handler
        private static void AlarmListenerWithPayload(object sender, AlarmEventArgs e)
        {
            Console.WriteLine("Alarm listener 1 called with location {0}", e.Location);
        }
        #endregion Alarm with payload

        #region Aggregating exceptions

        public static void AlarmWithAggregateException()
        {
            AggregateAlarm alarm = new AggregateAlarm();
            alarm.OnAlarmRaised += AlarmListenerWithAggregateExceptions1;
            alarm.OnAlarmRaised += AlarmListenerWithAggregateExceptions2;
            try
            {
                alarm.RaiseAlarm("Kitchen");
            }
            catch (AggregateException agg)
            {
                foreach (Exception ex in agg.InnerExceptions)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            Console.ReadKey();
        }

        private static void AlarmListenerWithAggregateExceptions1(object sender, AlarmEventArgs e)
        {
            Console.WriteLine("Alarm listener 1 called.");
            Console.WriteLine("Alarm in {0}.", e.Location);
            throw new Exception("Bang");
        }

        private static void AlarmListenerWithAggregateExceptions2(object sender, AlarmEventArgs e)
        {
            Console.WriteLine("Alarm listener 2 called.");
            Console.WriteLine("Alarm in {0}.", e.Location);
            throw new Exception("Boom");
        }
        #endregion Aggregating exceptions
    }

    #region Aggregate exceptions

    internal class AggregateAlarm
    {
        public event EventHandler<AlarmEventArgs> OnAlarmRaised = delegate { };

        //event
        public void RaiseAlarm(string location)
        {
            List<Exception> exceptionList = new List<Exception>();
            foreach (Delegate handler in OnAlarmRaised.GetInvocationList())
            {
                try
                {
                    handler.DynamicInvoke(this, new AlarmEventArgs(location));

                }
                catch (TargetInvocationException e)
                {
                    exceptionList.Add(e.InnerException);
                }
            }
            Console.WriteLine("Alarm raised!");
            if(exceptionList.Count > 0)
                throw new AggregateException(exceptionList);
        }
    }

    #endregion Aggregate exceptions

    #region Alarm with payload
    internal class AlarmEventArgs: EventArgs
    {
        public string Location { get; set; }

        public AlarmEventArgs(string location)
        {
            Location = location;
        }
    }

    internal class PayloadAlarm
    {
        public event EventHandler<AlarmEventArgs> OnAlarmRaised = delegate { };

        // event
        public void RaiseAlarm(string location)
        {
            OnAlarmRaised(this, new AlarmEventArgs(location));
        }
    }
    #endregion Alarm with payload

    internal class Alarm
    {
        // delegate for the alarm event
        public Action OnAlarmRaised { get; set; }

        // called to raise an alarm
        // event
        public void RaiseAlarm()
        {
            // only raise an alarm if someone has subscribed
            OnAlarmRaised?.Invoke();
        }
    }

    #region Alarm with event handler
    internal class EventHandlerAlarm
    {
        public event EventHandler OnAlarmRaised = delegate { };

        // event
        public void RaiseAlarm()
        {
            OnAlarmRaised(this, EventArgs.Empty);
        }
    }
    #endregion Alarm with event handler
}