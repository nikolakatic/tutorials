﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MsExam.Chapter1
{
    class MyFor:Work
    {
        public static void Execute()
        {
            int[] items = Enumerable.Range(0, 20).ToArray();

            //from-inclusive to-exclusive
            Parallel.For(0, items.Length, i =>
            {
                WorkOnItem(items[i]);
            });
            Console.WriteLine("Finished processing. Press any key to continue.");
            Console.ReadKey();
        }
    }
}
