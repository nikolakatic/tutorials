﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MsExam.Chapter1
{
    class Work
    {
        protected static void Task1()
        {
            Console.WriteLine("Task 1 starting");
            Thread.Sleep(2000);
            Console.WriteLine("\tTask 1 completed");
        }

        protected static void Task2()
        {
            Console.WriteLine("Task 2 starting");
            Thread.Sleep(1000);
            Console.WriteLine("\tTask 2 completed");
        }

        protected static void WorkOnItem(object item)
        {
            Random r = new Random();
            Console.WriteLine("Started working on item: {0}", item);
            Thread.Sleep(r.Next(100, 200));
            Console.WriteLine("\tFinished working on item: {0}", item);
        }

        protected static void DoWork()
        {
            Console.WriteLine("Work starting.");
            Thread.Sleep(new Random().Next(1000, 2000));
            Console.WriteLine("\tWork finished");
        }

        protected static void DoWork(int i)
        {
            Console.WriteLine("Task {0} starting", i);
            Thread.Sleep(new Random().Next(1000, 2000));
            Console.WriteLine("\tTask {0} finished", i);
        }

        protected static int CalculateResult()
        {
            Console.WriteLine("Work starting.");
            Thread.Sleep(new Random().Next(1000, 2000));
            Console.WriteLine("\tWork finished.");
            return 99;
        }

        protected static void Finished()
        {
            Console.WriteLine("Finished processing. Press any key to continue.");
            Console.ReadKey();
        }
    }
}
