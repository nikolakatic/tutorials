﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HelloWPF._3DataBinding
{
    /// <summary>
    /// Interaction logic for DataConversion.xaml
    /// </summary>
    public partial class DataConversion : Window
    {
        public DataConversion()
        {
            InitializeComponent();
            EmployeeConversion emp = new EmployeeConversion() { Name = "Jessica", Title = "Jones", StartDate = new DateTime(2001,7,10)};
            DataContext = emp;
        }
    }
}
