﻿using JoeCoffeeStore.StockManagement.Messages;
using JoeCoffeeStore.StockManagement.Model;
using JoeCoffeeStore.StockManagement.Services;
using JoeCoffeeStore.StockManagement.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace JoeCoffeeStore.StockManagement.ViewModel
{
    public class CoffeeDetailViewModel : BaseViewModel
    {
        private IDataService _coffeeDataService;
        private Coffee _selectedCoffee;

        public Coffee SelectedCoffee { get => _selectedCoffee; set { _selectedCoffee = value; RaisePropertyChanged(); } }

        public ICommand SaveCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand ChangeCommand { get; set; }

        public CoffeeDetailViewModel(IDataService dataService) {

            this._coffeeDataService = dataService;
            LoadCommands();

            Messenger.Default.Register<Coffee>(this, OnCoffeeReceived);
        }

        private void LoadCommands()
        {
            SaveCommand = new CustomCommand(SaveCoffee, CanSaveCoffee);
            DeleteCommand = new CustomCommand(DeleteCoffee, CanDeleteCoffee);
            ChangeCommand = new CustomCommand(ChangeCoffee, CanChangeCoffee);
        }

        private void OnCoffeeReceived(Coffee coffee)
        {
            SelectedCoffee = coffee;
        }

        private bool CanChangeCoffee(object obj)
        {
            return _selectedCoffee != null ? true : false;
        }

        private void ChangeCoffee(object obj)
        {
            _selectedCoffee.CoffeeName = "Something expensive";
            _selectedCoffee.Price = 1000;
        }

        private bool CanDeleteCoffee(object obj)
        {
            return _selectedCoffee != null ? true : false;
        }

        private void DeleteCoffee(object obj)
        {
            _coffeeDataService.DeleteCoffee(SelectedCoffee);
            Messenger.Default.Send(new UpdateListMessage());
        }

        private bool CanSaveCoffee(object obj)
        {
            return _selectedCoffee != null ? true : false;
        }

        private void SaveCoffee(object obj)
        {
            _coffeeDataService.UpdateCoffee(SelectedCoffee);
            Messenger.Default.Send(new UpdateListMessage());
        }
    }
}
