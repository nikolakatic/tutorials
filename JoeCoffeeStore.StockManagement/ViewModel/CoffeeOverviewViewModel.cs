﻿using JoeCoffeeStore.StockManagement.Extensions;
using JoeCoffeeStore.StockManagement.Messages;
using JoeCoffeeStore.StockManagement.Model;
using JoeCoffeeStore.StockManagement.Services;
using JoeCoffeeStore.StockManagement.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace JoeCoffeeStore.StockManagement.ViewModel
{
    public class CoffeeOverviewViewModel : BaseViewModel
    {
        private ObservableCollection<Coffee> _coffees;
        private Coffee _selectedCoffee;

        #region Services
        private IDataService _coffeeDataService;
        private readonly IDialogService _dialogService;
        #endregion Services

        public ObservableCollection<Coffee> Coffees { get => _coffees; set { _coffees = value; RaisePropertyChanged(); } }
        public Coffee SelectedCoffee { get => _selectedCoffee; set { _selectedCoffee = value; RaisePropertyChanged(); } }

        public ICommand EditCommand { get; set; }

        public CoffeeOverviewViewModel(IDataService dataService, IDialogService dialogService)
        {
            this._coffeeDataService = dataService;
            this._dialogService = dialogService;

            LoadData();
            LoadCommands();
            Messenger.Default.Register<UpdateListMessage>(this, OnUpdateListMessageReceived);
        }

        private void OnUpdateListMessageReceived(UpdateListMessage obj)
        {
            LoadData();
            _dialogService.CloseDialog();
        }

        private void LoadCommands()
        {
            EditCommand = new CustomCommand(EditCoffee, CanEditCoffee);
        }

        private bool CanEditCoffee(object obj)
        {
            return SelectedCoffee != null ? true : false;
        }

        private void EditCoffee(object obj)
        {
            Messenger.Default.Send(SelectedCoffee);
            _dialogService.ShowDialog();
        }

        private void LoadData()
        {
            //if (DesignerProperties.GetIsInDesignMode(new System.Windows.DependencyObject())) return;
            Coffees = _coffeeDataService.GetAllCoffees().ToObservableCollection();
        }
    }
}
