﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MsExam.Chapter1.Skill1._5
{
    public static class ExceptionTypes
    {
        public static void ExceptionObject()
        {
            try
            {
                Console.Write("Enter an integer: ");
                string numberText = Console.ReadLine();
                int result;
                result = int.Parse(numberText);
                Console.WriteLine("You entered {0}", result);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message + "\n");
                Console.WriteLine("Stacktrace: " + ex.StackTrace + "\n");
                Console.WriteLine("HelpLink: " + ex.HelpLink + "\n");
                Console.WriteLine("TargetSite: " + ex.TargetSite + "\n");
                Console.WriteLine("Source:" + ex.Source);
            }

            Console.ReadKey();
        }

        internal static void CreatingCustomExceptions()
        {
            try
            {
                throw new CalcException("Calc failed", CalcException.CalcErrorCodes.DivideByZero);
            }
            catch (CalcException ex)
            {
                Console.WriteLine("Error: {0}", ex.Error);
            }
            Console.ReadKey();
        }
    }

    class CalcException: Exception
    {
        public enum CalcErrorCodes
        {
            InvalidNumberText,
            DivideByZero
        }

        public CalcErrorCodes Error { get; set; }

        public CalcException(string message, CalcErrorCodes error): base(message)
        {
            Error = error;
        }
    }
}
